#include <vector>
#include <cstdlib>
#include <cstdio>

static constexpr size_t DATA_SIZE = 4096;

/// Takes two pointers to arrays of ints `in1` and `in2` of size `N`, computes
/// their element-wise sum and stores the result in `out`.
void vector_add(const int *in1, const int *in2, int *out, size_t N) {
  for (size_t i = 0; i < N; i++)
    out[i] = in1[i] + in2[i];
}

/// Initialize vector with `N` elements, all filled with zeros.
auto create_zero_vector(size_t N) {
  return std::vector<int>(N, 0);
}

/// Initialize vector with `N` elements, all initialized with increasing values.
auto create_iota_vector(size_t N) {
  std::vector<int> numbers(N);
  for (size_t i = 0; i < N; i++)
    numbers[i] = i;
  return numbers;
}

/// Check if the results match
bool check_result(const std::vector<int> &output) {
  bool match = true;
  for (size_t i = 0; i < output.size(); i++)
    match = match && output[i] == 4 * i;
  return match;
}

int main(int argc, char **argv) {
  auto in1 = create_iota_vector(DATA_SIZE);
  auto in2 = create_iota_vector(DATA_SIZE);
  auto in3 = create_iota_vector(DATA_SIZE);
  auto in4 = create_iota_vector(DATA_SIZE);

  auto out1 = create_zero_vector(DATA_SIZE);
  auto out2 = create_zero_vector(DATA_SIZE);
  auto out3 = create_zero_vector(DATA_SIZE);

  // TODO: How to parallelize this block?
  { 
    vector_add(&in1[0], &in2[0], &out1[0], DATA_SIZE);
    vector_add(&in3[0], &in4[0], &out2[0], DATA_SIZE);
    vector_add(&out1[0], &out2[0], &out3[0], DATA_SIZE);
  }

  bool match = check_result(out3);
  printf("Results match: %s\n", (match) ? "yes" : "no");
  return (match) ? EXIT_SUCCESS : EXIT_FAILURE;
}

Tutorial OMPC (WSCAD-2023)
================================================================================

Este repositório contém o material necessário para seguir o tutorial "Getting up and Running with the OpenMP Cluster Programming Model" apresentado no WSCAD 2023 em Porto Alegre, RS, Brasil.

> **Nota**
> É possível encontrar a apresentação [aqui][slides].

Imagem do Container
--------------------------------------------------------------------------------

A forma mais fácil de usar o OMPC é beixando uma imagem Docker do Dockerhub. A imagem contém o compilador e todas as bibliotecas necessárias para começar a usar o OMPC. Toda semana um CI automatizado cria e executa a última imagem (`latest`) com o commit mais recente no repositório do OMPC, para garantir seu funcionamento. É possível utilizar a imagem via Docker ou Singularity. 

Para baixar a imagem, execute o comando:

No docker:
```console
$ docker pull ompcluster/runtime:latest
```

Ou no singularity:
```console
$ singularity pull docker://ompcluster/runtime:latest
```

Depois da imagem ser baixada, você pode explorar o diretório do projeto e instanciar o container a partir da imagem usando o seguinte comando: 

No docker:
```console
$ docker run -it --rm -v $PWD:/root ompcluster/runtime:latest bash
```

Ou no singularity:
```console
singularity shell ./runtime_latest.sif
```

Para garantir que o container está funcionando apropriadamente, rode `clang --version` dentro do container e verifique a saída. Ela deve mostrar algo pareciso com isso: 
```
OmpCluster clang version 14.0.0 (git@gitlab.com:ompcluster/llvm-project.git 4f60767e575b6f97c269f5a3e66cc1d508d23d32)
Target: x86_64-unknown-linux-gnu
Thread model: posix
InstalledDir: /scratch/llvm/build-debug/bin
```

Compilando e Rodando os Programas
--------------------------------------------------------------------------------

O runtime do OMPC foi construído em cima da interface `libomptarget` do OpenMP do LLVM. Portanto, os desenvolvedores precisam da nossa versão do `clang` para compilar os programas.

A compilação é feita como se segue:
```console
$ clang -fopenmp -fopenmp-targets=x86_64-pc-linux-gnu program.cpp -o program
```

Após compilado, o programa deve ser executado usando o comando `mpirun` do MPI, passando o número de processos como argumento. Outras flags do `mpirun` podem ser usadas normalmente (*ex.* `--hostfile`).

```console
$ mpirun -np 3 ./program
```

Informações Adicionais 
--------------------------------------------------------------------------------

O código fonte do runtime do OMPC está no [Gitlab][gitlab]. Se você encontrar qualquer problemas ou comportamentos não esperados com o runtime, por favor nos avise criando um [issue][issues] no Gitlab.
A documentação do projeto pode ser acessada nessa [página][rtdocs].


<!----------------------------------- LINKS ----------------------------------->
[gitlab]: https://gitlab.com/ompcluster/llvm-project
[issues]: https://gitlab.com/ompcluster/llvm-project/-/issues
[rtdocs]: https://ompcluster.readthedocs.io
[slides]: https://docs.google.com/presentation/d/1jaegu3amMzvvKaZ_X4BiMpISy1heMClt/edit?usp=sharing&ouid=102317691153590429581&rtpof=true&sd=true

